\documentclass{article}

\usepackage[T2A]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[russian]{babel} 

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{physics}

\title{Сведение уравнений для \( \va{\Pi}_e \) и \( \va{\Pi}_m \) в однородной среде к уравнению Гельмгольца \\ (источник: Г.И. Макаров, В.В. Новиков, С.Т. Рыбачек "Распространение электромагнитных волн над земной поверхностью") } 




\begin{document}

\maketitle

\section{Уравнения Максвелла}

Процессы распространения электромагнитных волн, обладающих гармонической зависимостью от времени, в линейных изотропных немагнитных средах при пренебрежении пространственной дисперсией описываются системой уравнений Максвелла для комплексных амплитуд $\va{E}$ и $\va{H}$:
%
\begin{equation}
	\begin{split}
		&\curl{\va{E}} = i \omega \mu_0 \va{H}, \\
		&\curl{\va{H}} = -i \omega \varepsilon^{'} \va{E} + \va{j}_{ex}, \\
		&\div (\va{H}) = 0, \\
		&\div (\varepsilon^{'} \va{E}) = \rho_{ex},
	\end{split}
\end{equation}
%
где $\rho_{ex}$ и $\va{j}_{ex}$ объемная плотность сторонних зарядов и токов, $\varepsilon^{'} = \varepsilon + i \sigma / \omega$ - комплексная диэлектрическая проницаемость среды, обладающей диэлектрической проницаемостью $\varepsilon$ и проводимостью $\sigma$.

Введем вектор объемной плотности дипольного момента
%
\begin{equation}  
	\va{j}_{ex} = \frac{ \partial \va{p}_{ex} }{ \partial t }.
\end{equation}  
%
Откуда получаем 
%
\begin{equation}
	\va{j}_{ex} = - i \omega \va{p}_{ex}, \; \div (\va{p}_{ex}) = - \rho_{ex}.
\end{equation}
%
Отметим, что сторонние токи являются реальными электрическими токами. Поэтому можем обозначить $\va{p}_{ex} = \va{p}_e$, где $\va{p}_e$ - плотность дипольного электрического момента антенных токов.

В итоге получаем $E$-систему уравнений Максвелла
%
\begin{equation}
	\begin{split}
		&\curl{\va{E}_e} = i \omega \mu_0 \va{H}_e, \\
		&\curl \va{H}_e = - i \omega \varepsilon' \va{E}_e -i \omega \va{p}_e, \\
		&\div (\mu_0 \va{H}_e) = 0, \\
		&\div (\varepsilon' \va{E}_e + \va{p}_e) = 0.			
	\end{split}
\label{E-system}
\end{equation}

В уравнения (\ref{E-system}) входит плотность $\va{j}_{ex}$ электрических антенных токов. В ряде случаев оказывается удобно использовать фиктивные магнитные антенные токи, которые реально не существуют, а служат средством описания электромагнитных полей реальных электрических токов. Для введения магнитных токов положим в уравнениях (\ref{E-system})
%
\begin{equation}
	\va{E} = \va{E}^{'} - \frac{i}{\omega \varepsilon^{'}} \va{j}_{ex}.
\label{magnetic_E}
\end{equation}
%
Откуда получаем систему
%
\begin{equation}
	\begin{split}
		&\curl{\va{E}_m^{'}} = i \omega \mu_0 \va{H}_m + \va{j}_m, \\
		&\curl{\va{H}_m} = -i \omega \varepsilon' \va{E}_m^{'}, \\
		&\div (\mu_0 \va{H}_m) = 0, \\
		&\div (\varepsilon' \va{E}_m^{'}) = 0.
	\end{split}
\label{new_Maxwell_eq}
\end{equation}
% 
где
%
\begin{equation}
	\va{j}_m = \frac{ i }{ \omega } \curl{ \frac{ \va{j}_{ex} }{ \varepsilon' } }.
\label{magnetic_current}
\end{equation}
%
Величину $\va{j}_m$ можно назвать плотностью магнитного (антенного) тока по аналогии с плотностью электрического антенного тока в системе (\ref{E-system}). Согласно уравнениям (\ref{new_Maxwell_eq}) магнитный ток с плотностью (\ref{magnetic_current}) создает такое же магнитное поле во всем пространстве и электрическое поле вне области расположения источников, что и истинные электрический антенный ток. В области же расположения источников электрические поля $\va{E}$ и $\va{E}^{'}$ этих токов связаны соотношением (\ref{magnetic_E}). Введем также объемную плотность магнитного дипольного момента
%
\begin{equation}
	\va{j}_m = -i \omega \va{p}_m. 
\end{equation}
%
Откуда получаем
%
\begin{equation}
	\va{p}_m = - \frac{1}{\omega^2} \curl{ ( \frac{ \va{j}_{ex} }{ \varepsilon' } ) } = \frac{i}{\omega} \curl{ ( \frac{ \va{p}_{ex} }{ \varepsilon' } ) }.
\end{equation}

Запишем магнитную М-систему уравнений Максвелла:
\begin{equation}
	\begin{split}
		&\curl{\va{E}'_m} = i \omega \mu_0 \va{H}_m - i \omega \va{p}_m, \\
		&\curl \va{H}_m = - i \omega \varepsilon' \va{E}'_m, \\
		&\div (\mu_0 \va{H}_m) = 0, \\
		&\div (\varepsilon' \va{E}'_m + \va{p}_m) = 0.		
	\end{split}
\label{M-system2}
\end{equation}
%
При получении системы (\ref{M-system2}) было использовано известное свойство дифференциальных операторов $\div (\curl{ \va{A} } ) = 0$.

Е- и М-системы уравнений Максвелла обладают свойством симметрии (принцип перестановочной двойственности):
%
\begin{equation}
\label{duality}
\vec{E}'_m \leftrightarrow \vec{H}_e; \; \vec{H}_m \leftrightarrow \vec{E}_e; \; \vec{p}_m \leftrightarrow \vec{p}_e; \; \mu_0 \leftrightarrow -\varepsilon'.
\end{equation}
%
При выполнении замены (\ref{duality}) уравнения (\ref{M-system2}) переходят в (\ref{E-system}).

\section{Вектор Герца}

При решении уравнений, описывающих электромагнитные поля, иногда целесообразно вводить те или иные потенциалы полей. Мы будем в дальнейшем использовать векторы Герца. 

\subsection{Электрический вектор Герца $\va{\Pi}_e$ }
Опираясь на соленоидальность магнитного поля $( \div \va{H}_e = 0 )$, выразим $\va{H}_e$ через электрический вектор Герца
%
\begin{equation}
	\va{H}_e = - i \omega \curl{\va{\Pi}_e}.
\label{Hertz_H}
\end{equation}
%
Из второго уравнения системы (\ref{E-system}) при этом получаем
%
\begin{equation}
	\va{E}_e = \frac{1}{\varepsilon'} \curl{ \curl{ \va{\Pi}_e } } - \frac{ \va{p}_e }{ \varepsilon' }.
\label{Hertz_E}
\end{equation}
%
Подставляя (\ref{Hertz_H}) и (\ref{Hertz_E}) в первое уравнение системы (\ref{E-system}), получаем
%
\begin{equation}
	\curl{ ( \frac{1}{\varepsilon'} \curl{ \curl{ \va{\Pi}_e }} - \frac{\va{p}_e}{\varepsilon'} - \omega^2 \mu_0 \va{\Pi}_e ) } = 0.
\end{equation}
%
Используем свойство дифференциальных операторов $\curl{(\grad \Phi_e)} = 0$: 
%
\begin{equation}
	\curl{ \curl{ \va{\Pi}_e } } - k^2 \va{\Pi}_e - \va{p}_e = \varepsilon' \grad \Phi_e,
\end{equation}
%
где $k^2 = \omega^2 \varepsilon_m^{'} / c^2$, $c = 1/ \sqrt{\varepsilon_0 \mu_0} $, $\Phi_e$ - произвольная гладкая функция.

\subsection{Магнитный вектор Герца $\va{\Pi}_m$}

Магнитный вектор Герца введем исходя из уравнений (\ref{M-system2}). Учитывая соленоидальность вектора $\varepsilon' \va{E}_m^{'}$, положим
%
\begin{equation}
	\va{E}_m^{'} = -i \frac{\omega}{\varepsilon_m^{'}} \curl{ \va{\Pi}_m }.
\label{Hertz_Em}
\end{equation}
%
Из первого уравнения системы (\ref{M-system2}) получаем
%
\begin{equation}
	\va{H}_m = - \frac{1}{\mu_0} \curl{ ( \frac{1}{\varepsilon_m^{'}} \curl{ \va{\Pi}_m } ) } + \frac{ \va{p}_m }{\mu_0}.
\label{Hertz_Hm}
\end{equation}
%
Подставляя (\ref{Hertz_Em}) и (\ref{Hertz_Hm}) в первое уравнение (\ref{M-system2}), получаем уравнение для вектора Герца 
%
\begin{equation}
	\curl{ ( \frac{1}{\varepsilon_m^{'}} \curl{ \va{\Pi}_m } ) } - k_0^2 \va{\Pi}_m - \va{p}_m = \mu_0 \grad \Phi_m,
\end{equation}
%
где $k_0^2 = \omega^2 / c^2$, $\Phi_m$ - произвольная гладкая функция.

% \subsection{Важные результаты}

% \paragraph{\(E\)-система уравнений Максвелла}

% \begin{equation}
% 	\begin{split}
% 		&\curl{\va{E}_e} = i \omega \mu_0 \va{H}_e, \\
% 		&\curl \va{H}_e = - i \omega \varepsilon' \va{E}_e -i \omega \va{p}_e, \\
% 		&\div (\mu_0 \va{H}_e) = 0, \\
% 		&\div (\varepsilon' \va{E}_e + \va{p}_e) = 0,			
% 	\end{split}
% %\label{E-system}
% \end{equation}
% %
% где
% \begin{equation*}
% \varepsilon' = \varepsilon+\frac{i\sigma}{\omega} \text{ -  комплексная диэлектрическая проницаемость}.
% \end{equation*}
% %\begin{align}
% %	\varepsilon' &= \varepsilon+\frac{i\sigma}{\omega} \text{ -  комплексная диэлектрическая проницаемость} \nonumber \\
% %	\va{p}_e &= \frac{i}{\omega} \va{j}_{\text{ст}} \text{ - вектор объёмной плотности дипольного момента} \nonumber
% %\end{align}

% %
% Правило введения электрического вектора Герца:
% \begin{equation}
% 	\begin{split}
% 		\va{E}_e &= \frac{1}{\varepsilon'} \curl \curl \va{\Pi}_e - \frac{\va{p}_e}{\varepsilon'}, \\
% 		\va{H}_e &= -i \omega \curl \va{\Pi}_e,
% 	\end{split}
% \end{equation}
% %
% \begin{equation}
% 	\curl \curl \va{\Pi}_e - k^2 \va{\Pi}_e - \va{p}_e = \varepsilon' \grad \Phi_e,
% \end{equation}
% где \(\varepsilon'_m = \varepsilon' / \varepsilon_0 \), \(k^2 = \omega^2 \varepsilon_0 \mu_0 \varepsilon'_m\), \(\Phi_e\) --- произвольная гладкая функция.

% \paragraph{\(M\)-система уравнений Максвелла}

% \begin{equation}
% 	\begin{split}
% 		&\curl{\va{E}'_m} = i \omega \mu_0 \va{H}_m - i \omega \va{p}_m, \\
% 		&\curl \va{H}_m = - i \omega \varepsilon' \va{E}'_m, \\
% 		&\div (\mu_0 \va{H}_m) = 0, \\
% 		&\div (\varepsilon' \va{E}'_m + \va{p}_m) = 0.		
% 	\end{split}
% \label{M-system}
% \end{equation}

% %
% Правило введения магнитного вектора Герца:
% \begin{equation}
% 	\begin{split}
% 		\va{E}'_m &=  -\frac{i \omega}{\varepsilon'_m} \curl \va{\Pi}_m, \\
% 		\va{H}_m &= \frac{\va{p}_m}{\mu_0} - \frac{1}{\mu_0} \curl( \frac{1}{\varepsilon'_m} \curl \va{\Pi}_m),
% 	\end{split}
% \end{equation}
% %
% %
% \begin{equation}
% \label{eq:Pi_m_first}
% 	\curl( \frac{1}{\varepsilon'_m} \curl \va{\Pi}_m) - k_0^2 \va{\Pi}_m - \va{p}_m = \mu_0 \grad \Phi_m,
% \end{equation}
% где $\va{E}'_m = \va{E}_m+\va{p}_{\text{ст}}/\varepsilon'$, $\va{p}_m=i\curl{\left(\va{p}_{\text{ст}}/\varepsilon'\right)}/\omega$, \(k_0^2 = \omega^2 \varepsilon_0 \mu_0 \), \(\Phi_m\) --- произвольная гладкая функция.

\section{Координата разделения}

Упрощение математического описания достигается в случае использования кроволинейной ортогональной системы координат \((\xi, \eta, \zeta)\), обладающей координатой разделения. Пусть \(\zeta\) -- координата разделения. Тогда по определению
\begin{equation}
	\begin{split}
	&h_{\zeta} = h_{\zeta} (\zeta), \\
	&\pdv{\zeta} \qty(\frac{h_\xi}{h_\eta}) = 0,
	\end{split}
\label{Lame}
\end{equation}
где \(h_{\xi,\eta,\zeta}\) -- коэффициенты Ламе:
\begin{equation}
	h_i = \qty[\qty(\pdv{x}{i})^2 + \qty(\pdv{y}{i})^2 + \qty(\pdv{z}{i})^2]^{1/2}, \quad i=\xi,\eta,\zeta
\end{equation}

Примеры
\begin{enumerate}
	\item Декартова система координат
		\[ 
			\mqty(\xi \\ \eta \\ \zeta ) =
			\mqty(x \\ y \\ z) \Rightarrow
			h_{\xi,\eta,\zeta} = 1.
		\]
		Координата разделения -- любая.
	\item Цилиндрическая система координат
		\[ 
			\mqty(\xi \\ \eta \\ \zeta) =
			\mqty(\rho \\ \varphi \\ z)
		\]
		\[
			\mqty{x = \rho \cos \varphi \\ y = \rho \sin \varphi \\ z = z} \Rightarrow
			\mqty{h_\rho = 1 \\ h_\varphi = \rho \\ h_z = 1}
		\]
		Координата разделения -- \(z\)
	\item Сферическая система координат 
		\[ 
			\mqty(\xi \\ \eta \\ \zeta ) =
			\mqty(r \\ \theta \\ \varphi) 
		\]
		\[
			\mqty{
				x = r \cos \varphi \sin \theta \\
				y = r \sin \varphi \sin \theta \\
				z = r \cos \theta
			} \Rightarrow
			\mqty{h_r = 1 \\ h_\theta = r \sin \theta \\ h_\varphi = r}
		\]
		Координата разделения -- \(r\).
\end{enumerate}

В однородной среде при выполнении условий существования координаты разделения в области вне источников общее решение уравнений Максвелла дается суперпозицией двух фундаментальных решений, одно из которых описывается однокомпонентным электрическим, а другое - однокомпонентным магнитным векторами Герца, направленными по координате разделения. Первое из этих решений является поперечно-магнитным (ТМ или e), а второе - поперечно-электрическим (ТЕ или m) относительно координаты разделения. Наличие у уравнений Максвелла двух фундаментальных решений, соответствующих ТМ- и ТЕ-полям, называется принципом поляризационной двойственности. Далее мы обобщим этот принцип на случай неоднородной вдоль координаты разделения среды.

E-система уравнений и M-система связаны через соответствующее деление источника. Источник известен, т.е. задано значение $\va{j}_{\text{ст}}$. Далее вводится вектор объёмной плотности дипольного момента: $\va{p}_{\text{ст}}=\va{p}_{\text{e}}=i\va{j}_\text{ст}/\omega$. Источник может возбуждать как ТМ-поля (E-поля), так и ТЕ-поля (М-поля). Таким образом делим источники на $\va{p}_{\text{e}}^{\text{e}}$ (для Е-поля) и $\va{p}_{\text{e}}^{\text{m}}$ (для М-поля): $\va{p}_{\text{e}} = \va{p}_{\text{e}}^{\text{e}} + \va{p}_{\text{e}}^{\text{m}}.$ Таким образом, в Е-системе уравнений $\va{p}_e$ соответствует $\va{p}_e^e$. В М-системе $\va{p}_m$ соответствует $\va{p}_m^m$, где $\va{p}_m^m = \frac{i}{\omega} \curl{\frac{\va{p}_e^m}{\varepsilon'}}$.

В итоге, запишем уравнения для электрического и магнитного вектора Герца с учетом разделения источников в следующем виде:
\begin{align}
&\curl \curl \va{\Pi}_e - k^2 \va{\Pi}_e - \va{p}_e^e = \varepsilon' \grad \Phi_e, \\
&\curl( \frac{1}{\varepsilon'_m} \curl \va{\Pi}_m) - k_0^2 \va{\Pi}_m - \va{p}_m^m = \mu_0 \grad \Phi_m, \label{eq:Pi_m1} \\
&k_0^2 = \frac{\omega^2}{c^2}, \; k^2 = \frac{\omega^2}{c^2} \varepsilon'_m, \; \varepsilon'_m = \frac{\varepsilon'}{\varepsilon_0}.
\end{align}

\section{Вид дифференциальных операторов в криволинейной ортогональной СК}

\begin{equation}
	\grad \Phi =
	\frac{1}{h_\xi} \pdv{\Phi}{\xi} \vb{e}_\xi +
	\frac{1}{h_\eta} \pdv{\Phi}{\eta} \vb{e}_\eta + 
	\frac{1}{h_\zeta} \pdv{\Phi}{\zeta} \vb{e}_\zeta
\end{equation}
%
\begin{equation}
	\div \vb{A} = \frac{1}{h_\xi h_\eta h_\zeta} \qty[
	\pdv{\xi} \qty(h_\eta h_\zeta A_\xi) +
	\pdv{\eta} \qty(h_\xi h_\zeta A_\eta) + 
	\pdv{\zeta} \qty(h_\xi h_\eta A_\zeta)
	]
\end{equation}
%
\begin{equation}
	\curl \vb{A} = 
	\qty(\curl \vb{A})_\xi \vb{e}_\xi + 
	\qty(\curl \vb{A})_\eta \vb{e}_\eta + 
	\qty(\curl \vb{A})_\zeta \vb{e}_\zeta 
\end{equation}
%
\begin{equation}
	\qty(\curl \vb{A})_\xi = \frac{1}{h_\eta h_\zeta} \qty[
	\pdv{\eta} \qty(h_\zeta A_\zeta) -
	\pdv{\zeta} \qty(h_\eta A_\eta)
	] 
\end{equation}
%
\begin{equation}
	\qty(\curl \vb{A})_\eta = \frac{1}{h_\xi h_\zeta} \qty[
	\pdv{\zeta} \qty(h_\xi A_\xi) -
	\pdv{\xi} \qty(h_\zeta A_\zeta)
	] 
\end{equation}
%
\begin{equation}
	\qty(\curl \vb{A})_\zeta = \frac{1}{h_\xi h_\eta} \qty[
	\pdv{\xi} \qty(h_\eta A_\eta) -
	\pdv{\eta} \qty(h_\xi A_\xi)
	] 
\end{equation}
%
\begin{equation}
	\Delta \Phi = \frac{1}{h_\xi h_\eta h_\zeta} \qty[
	\pdv{\xi} \qty(\frac{h_\eta h_\zeta}{h_\xi} \pdv{\Phi}{\xi}) + 
	\pdv{\eta} \qty(\frac{h_\xi h_\zeta}{h_\eta} \pdv{\Phi}{\eta}) + 
	\pdv{\zeta} \qty(\frac{h_\xi h_\eta}{h_\zeta} \pdv{\Phi}{\zeta})
	]
\end{equation}


\section{Принцип поляризационной двойственности}

В криволинейной ортогональной системе координат с координатой разделения $\zeta$ в однородной среде вне источников общее решение уравнений Максвелла дается суперпозицией 2-х фундаментальных решений. Каждое решение описывается однокомпонентным вектором Герца, направленным по координате разделения:
\begin{enumerate}
	\item поперечно-магнитное решение (TM или e-решение) --- \emph{электрический} однокомпонентный вектор Герца;
	\item поперечно-электрическое решение (TE или m-решение) --- \emph{магнитный} однокомпонентный вектор Герца.
\end{enumerate}

Уравние для $\va{\Pi}_e$
\begin{equation}
	\label{eq:Pi_e}
	\curl \curl \va{\Pi}_e - k^2 \va{\Pi}_e - \va{p}_e^e = \varepsilon' \grad \Phi_e.
\end{equation}
Так как $\zeta$ --- координата разделения
\begin{equation}
	\va{\Pi}_e = \Pi_e \va{e}_\zeta.
\end{equation}
Отсюда
\begin{equation}
	\begin{split}
		&\qty(\curl \va{\Pi}_e)_\xi =
		\frac{1}{h_\eta h_\zeta} \pdv{\eta} \qty(h_\zeta \Pi_e) \\
		&\qty(\curl \va{\Pi}_e)_\eta = -
		\frac{1}{h_\xi h_\zeta} \pdv{\xi} \qty(h_\zeta \Pi_e) \\
		&\qty(\curl \va{\Pi}_e)_\zeta = 0
	\end{split}
\end{equation}
Уравнение (\ref{eq:Pi_e}), записанное покомпонентно (в проекциях на орты $\va{e}_\xi$, $\va{e}_\eta$, $\va{e}_\zeta$), становится
 \begin{equation}
	\begin{split}
		&\qty(\curl \curl \va{\Pi}_e)_\xi
		- p^e_{e \xi}
		= \varepsilon' \frac{1}{h_\xi} \pdv{\Phi}{\xi} \\
		&\qty(\curl \curl \va{\Pi}_e)_\eta
		- p^e_{e \eta}
		= \varepsilon' \frac{1}{h_\eta} \pdv{\Phi}{\eta} \\
		&\qty(\curl \curl \va{\Pi}_e)_\zeta
		-k^2 \Pi_e - p^e_{e \zeta}
		= \varepsilon' \frac{1}{h_\zeta} \pdv{\Phi}{\zeta} \\
	\end{split}
\end{equation}
Далее считаем, что выбранная система координат такова, что $h_\zeta=1$. В полученной системе распишем выражения для роторов
%TODO: Есть мнение, что в 3 уравнении что-то не так с размерностью
\begin{align}
	\label{eq:simple_Pi_e_xi}
	&\frac{1}{h_\xi} \pdv{\Pi_e}{\zeta}{\xi}- p^e_{e \xi}
	= \frac{\varepsilon'}{h_\xi}\pdv{\Phi_e}{\xi} \\	
	\label{eq:simple_Pi_e_eta}
	&\frac{1}{h_\eta} \pdv{\Pi_e}{\zeta}{\eta} - p^e_{e \eta}
	= \frac{\varepsilon'}{h_\eta}\pdv{\Phi_e}{\eta} \\
	\label{eq:simple_Pi_e_zeta}
	&\frac{1}{h_\xi h_\eta} \qty[- \pdv{\xi} \qty( \frac{h_\eta}{h_\xi} \pdv{\Pi_e}{\xi}) 
	- \pdv{\eta} \qty(\frac{h_\xi}{h_\eta} \pdv{\Pi_e}{\eta}) ]
	- k^2 \Pi_e - p^e_{e \zeta}
	= \varepsilon' \pdv{\Phi_e}{\zeta}
\end{align}
Уравнение (\ref{eq:simple_Pi_e_xi}) умножаем на $h_\xi$ и берем производную  $\pdv{\eta}$. Получаем
\begin{equation}
	\label{eq:simple_Pi_e_xi_mod}
	\frac{\partial^3 \Pi_e}{\partial \xi \partial \eta \partial \zeta}
	= \pdv{\eta} \qty(h_\xi p^e_{e \xi}) + \varepsilon' \pdv{\Phi_e}{\eta}{\xi}.
\end{equation}
Уравнение (\ref{eq:simple_Pi_e_eta}) умножаем на $h_\eta$ и берем производную  $\pdv{\xi}$. Получаем
\begin{equation}
	\label{eq:simple_Pi_e_eta_mod}
	\frac{\partial^3 \Pi_e}{\partial \xi \partial \eta \partial \zeta}
	= \pdv{\xi} \qty(h_\eta p^e_{e \eta}) + \varepsilon' \pdv{\Phi_e}{\eta}{\xi}.
\end{equation}
Отсюда, вычитая из уравнения (\ref{eq:simple_Pi_e_xi_mod}) уравнение (\ref{eq:simple_Pi_e_eta_mod}), получаем
\begin{equation}
	\label{eq:pe_condition}
	\pdv{\eta} \qty(h_\xi p^e_{e \xi}) = \pdv{\xi} \qty(h_\eta p^e_{e \eta})
\end{equation}

Заметим, что условие (\ref{eq:pe_condition}) будет выполнено, если ввести новую гладкую функцию $Q$ такую, что
\begin{equation}
	\label{eq:Q_definition}
	\begin{split}	
	    &p^e_{e \xi} = \frac{1}{h_\xi} \pdv{Q}{\xi}, \\
	    &p^e_{e \eta} = \frac{1}{h_\eta} \pdv{Q}{\eta}.
	\end{split}
\end{equation}
Учтем (\ref{eq:Q_definition}) в уравнениях (\ref{eq:simple_Pi_e_xi}, \ref{eq:simple_Pi_e_eta})
\begin{align}
	&\frac{1}{h_\xi} \pdv{\Pi_e}{\xi}{\zeta}
	= \frac{1}{h_\xi} \qty(\pdv{Q}{\xi} + \varepsilon' \pdv{\Phi_e}{\xi}) \\
	&\frac{1}{h_\eta} \pdv{\Pi_e}{\eta}{\zeta}
	= \frac{1}{h_\eta} \qty(\pdv{Q}{\eta} + \varepsilon' \pdv{\Phi_e}{\eta})
\end{align}
Или, учитывая зависимость диэлектрической проницаемости только от координаты разделения ($\varepsilon' = \varepsilon'(\zeta)$),
\begin{align}
	&\pdv{\Pi_e}{\xi}{\zeta} = \pdv{\xi} \qty(Q + \varepsilon' \Phi_e) \\
	&\pdv{\Pi_e}{\eta}{\zeta} = \pdv{\eta} \qty(Q + \varepsilon' \Phi_e).
\end{align}
Отсюда
\begin{align}
	&\pdv{\xi} \qty(\pdv{\Pi_e}{\zeta} - Q - \varepsilon' \Phi_e) = 0 \\
	&\pdv{\eta} \qty(\pdv{\Pi_e}{\zeta} - Q - \varepsilon' \Phi_e) = 0.
\end{align}
В итоге получаем
\begin{equation}
	\pdv{\Pi_e}{\zeta} - Q - \varepsilon' \Phi_e - \Psi(\zeta) = 0, 
\end{equation}
где $\Psi$ --- функция только от $\zeta$. Поскольку  $\Pi_e$ входит в выражения для полей  $\va{E}_e$ и  $\va{H}_e$ только через производные $\partial \Pi_e /  \partial \xi $, $\partial \Pi_e / \partial \eta$, $\partial^2 \Pi_e / \left( \partial \xi \partial\zeta \right)$ и $\partial^2 \Pi_e / \left( \partial \eta \partial \zeta \right)$, функцию  $\Psi$ можно считать нулем. Тогда
\begin{equation}
	\pdv{\Pi_e}{\zeta} - Q - \varepsilon' \Phi_e = 0.
\end{equation}
Отсюда
\begin{equation}
	\Phi_e = \frac{1}{\varepsilon'} \pdv{\Pi_e}{\zeta} - \frac{Q}{\varepsilon'}.
\end{equation}
Тогда
\begin{multline}
	\pdv{\Phi_e}{\zeta} 
	= \pdv{\zeta} \qty(\frac{1}{\varepsilon'} \pdv{\Pi_e}{\zeta} - \frac{Q}{\varepsilon'}) = \\ = \frac{1}{\varepsilon'} \frac{\partial^2 \Pi_e}{\partial \zeta^2} + \frac{d}{d\zeta}\left(\frac{1}{\varepsilon'}\right)\frac{\partial \Pi_e}{\partial \zeta} - \frac{\partial}{\partial \zeta} \left(\frac{Q}{\varepsilon'}\right).
%	- \frac{1}{\varepsilon'^2} \dv{\varepsilon'}{\zeta} \pdv{\Pi_e}{\zeta}
%	+ \frac{1}{\varepsilon'} \pdv[2]{\Pi_e}{\zeta}
%	+ \frac{1}{\varepsilon'^2}\dv{\varepsilon'}{\zeta} Q 
%	- \frac{1}{\varepsilon'} \pdv{Q}{\zeta} = \\
%	= \frac{1}{\varepsilon'} \pdv[2]{\Pi_e}{\zeta}
%	- \frac{1}{\varepsilon'} \pdv{Q}{\zeta}
%	- \frac{1}{\varepsilon'} \dv{\varepsilon'}{\zeta} \Phi_e.
\end{multline}

Перепишем уравнение (\ref{eq:simple_Pi_e_zeta}) с учетом вышесказанного
%\begin{equation}
%	\Delta_{\xi \eta} \Pi_e + k^2 \Pi_e + p^e_{e \zeta} = 
%	\dv{\varepsilon'}{\zeta} \Phi_e - \pdv[2]{\Pi_e}{\zeta} + \pdv{Q}{\zeta}.
%\end{equation}
%Тогда
\begin{equation}
	\label{eq:ya_Pi_e}
	\qty(\Delta_{\xi \eta} + \pdv[2]{\zeta}) \Pi_e + k^2 \Pi_e
	+ \varepsilon' \dv{\zeta} \qty(\frac{1}{\varepsilon'}) \pdv{\Pi_e}{\zeta}
	= -p^e_{e \zeta} + \varepsilon' \pdv{\zeta} \qty(\frac{Q}{\varepsilon'}).
\end{equation}
%
%Как видно, в однородной среде полученное уравнение принимает вид уравнения Гельмгольца.

Аналогичные рассуждения можно провести для магнитного вектора Герца. Введем новый магнитный вектор Герца
\begin{equation}
\va{\Pi}'_m = \frac{\va{\Pi}_m}{\varepsilon'_m}.
\end{equation}
Компоненты полей выражаются через $\va{\Pi}'_m$ следующим образом через $\va{\Pi}'_m$:
\begin{equation*}
\va{E}'_m = -i\omega \curl{\va{\Pi}'_m}, \; \va{H}_m = -\frac{1}{\mu_0} \curl{\curl{\va{\Pi}'_m}} + \frac{1}{\mu_0} \va{p}^m_m.
\end{equation*}
%
Перепишем уравнение (\ref{eq:Pi_m1}) для введенного вектора $\va{\Pi}'_m$
\begin{equation}
\curl{\curl{\va{\Pi}'_m}} - k^2 \va{\Pi}'_m - \va{p}_m^m = \mu_0 \grad{\Phi}_m.
\end{equation}
%
Сравнивая вид полученного уравнения с видом уравнения для $\va{\Pi}_e$ (\ref{eq:Pi_e}), сразу получаем
\begin{equation}
\label{eq:ya_Pi_m}
\qty(\Delta_{\xi \eta} + \pdv[2]{\zeta}) \Pi'_m + k^2 \Pi'_m
= -p^m_{m \zeta} + \frac{\partial M}{\partial \zeta},
\end{equation}
%
где функция $M$ определяется условиями
\begin{equation}
\begin{split}
&p^m_{m \xi} = \frac{1}{h_\xi} \pdv{M}{\xi},\\
&p^m_{m \eta} = \frac{1}{h_\eta} \pdv{M}{\eta}.
\end{split}
\label{M-function}
\end{equation}

Отметим, что функции Q и M в уравнениях (\ref{eq:ya_Pi_e}) и (\ref{eq:ya_Pi_m}) остаются неизвестными. 

\subsection{Определений функций Q и M}

Как найти функции $Q$ и $M$ в уравнениях для векторов Герца? Функции $Q$ и $M$ определяются из разделения источников. Источники можно разделить на $e$- и $m$-источники: $\va{p}_e=\va{p}_e^e+\va{p}_e^m$. При этом выполнено, что $$\va{p}_m^m=\frac{i}{\omega} \curl{\frac{\left(\va{p}_e-\va{p}_e^e\right)}{\varepsilon'}}.$$ Пусть $p^m_{e\zeta}=0$. Тогда $p^e_{e\zeta}=p_{e\zeta}$. 

Хотим найти связь между функицей Q и проекциями источника $p_{e\xi}$, $p_{e\eta}$. Уже известно, что  
%
\begin{align}
\label{m_source_xi}
p_{m\xi}^m &= -\frac{i}{\omega h_{\eta}} \frac{\partial}{\partial \zeta} \left( \frac{h_\eta}{\varepsilon'} \left( p_{e\eta} - p_{e\eta}^e \right) \right), \\
p_{m\eta}^m &= \frac{i}{\omega h_{\xi}} \frac{\partial}{\partial \zeta} \left( \frac{h_\xi}{\varepsilon'} \left( p_{e\xi} - p_{e\xi}^e \right) \right).
\label{m_source_eta}
\end{align}
%
Умножим (\ref{m_source_xi}) на $h_\xi$ и от полученного возьмем производную $\partial / \partial \eta$
%
\begin{equation}
\frac{\partial}{\partial \eta} \left( h_\xi p_{m\xi}^m \right) = -\frac{i}{\omega} \frac{\partial}{\partial \eta} \left[ \frac{ h_\xi }{ h_\eta } \frac{\partial}{\partial \zeta} \left( \frac{h_\eta}{\varepsilon'} \left( p_{e\eta} - p_{e\eta}^e \right) \right) \right].
\label{Q_s1}
\end{equation}
%
Уравнение (\ref{m_source_eta}) умножим на $h_\eta$ и от полученного возьмем производную $\partial / \partial \xi$
%
\begin{equation}
\frac{\partial}{\partial \xi} \left( h_\eta p_{m\eta}^m \right) = \frac{i}{\omega} \frac{\partial}{\partial \xi} \left[ \frac{ h_\eta }{ h_\xi } \frac{\partial}{\partial \zeta} \left( \frac{h_\eta}{\varepsilon'} \left( p_{e\xi} - p_{e\xi}^e \right) \right) \right].
\label{Q_s2}
\end{equation}
%
В получившихся равенствах (\ref{Q_s1}), (\ref{Q_s2}) воспользуемся связью функции Q с e-источниками (\ref{eq:pe_condition}). И, наконец, из полученного ранее равенства (\ref{Q_s1}) и (\ref{Q_s2}), выводим уравнение на функцию $Q$
\begin{equation}
\laplacian_{\xi\eta}{Q} = \frac{1}{h_\xi h_\eta} \left( \frac{\partial}{\partial \eta} \left(h_\xi p_{e\eta}\right) + \frac{\partial}{\partial \xi} \left(h_\eta p_{e\xi}\right) \right).
\label{Q-equation}
\end{equation}
%
Далее придерживаемся следующего алгоритма рассуждений: $p_{e\xi}$ и $p_{e\eta}$ известны, так как известен источник $\va{p}_e$. Тогда из полученного уравнения (\ref{Q-equation}) находим $Q$. Зная $Q$ и используя (\ref{eq:Q_definition}), находим $p_{e\xi}^e$ и $p_{e\eta}^e$. Ранее было отмечено, что $p^e_{e\zeta}=p_{e\zeta}$. Таким образом, нашли $\va{p}_e^e$. Теперь можем найти $\va{p}_e^m$ ($\va{p}_e^m=\va{p}_e - \va{p}_e^e$). И можем найти $\va{p}_m^m$ по правилу $\va{p}_m^m=\frac{i}{\omega} \curl{\frac{\left(\va{p}_e-\va{p}_e^e\right)}{\varepsilon'}}$. Зная $\va{p}_m^m$ и используя (\ref{M-function}), находим функцию $M$. Таким образом, целиком определили правые части уравнений (\ref{eq:ya_Pi_e}) и (\ref{eq:ya_Pi_m}) для потенциалов Герца. 

\section{Упрощение уравнений для векторов Герца}

В предыдущем разделе было получено уравнение для электрического вектора Герца
\begin{equation}
%\label{eq:ya_Pi_e}
\qty(\Delta_{\xi \eta} + \pdv[2]{\zeta}) \Pi_e + k^2 \Pi_e
+ \varepsilon' \dv{\zeta} \qty(\frac{1}{\varepsilon'}) \pdv{\Pi_e}{\zeta}
= -p^e_{e \zeta} + \varepsilon' \pdv{\zeta} \qty(\frac{Q}{\varepsilon'}).
\end{equation}

Поскольку $\zeta$ --- координата разделения можно коэффициенты Ламе  $h_\xi$ и  $h_\eta$ представить следующим образом
\begin{equation}
	\begin{split}
	&h_\xi = T(\zeta) \alpha(\xi, \eta), \\
	&h_\eta = T(\zeta) \beta(\xi, \eta), \\
	&h_\zeta = 1.
	\end{split}
\end{equation}
При таком выборе вида коэффициентов Ламэ выполнены условия (\ref{Lame}) для координаты разделения $\zeta$.

Будем искать решение уравнения (\ref{eq:ya_Pi_e}) в виде $\Pi_e=T(\zeta) U(\xi, \eta, \zeta)$. Тогда уравнение (\ref{eq:ya_Pi_e}) переписывается следующим образом
\begin{multline}
T \frac{\partial^2 U}{\partial \zeta^2} + U \frac{d^2T}{d\zeta^2} + 2 \frac{\partial U}{\partial \zeta} \frac{dT}{d\zeta} + T \laplacian_{\xi\eta}{U} + k^2 TU + \varepsilon' \dv{\zeta} \qty(\frac{1}{\varepsilon'}) \pdv{\left(TU\right)}{\zeta}  \\ = -p^e_{e\zeta} + \varepsilon' \pdv{\zeta} \qty(\frac{Q}{\varepsilon'}).
\end{multline}
%
Разделим полученное уравнение на $T\left(\zeta\right)$ и воспользуемся определением оператора Лапласа в криволинейной ортогональной системе координат 
%\begin{multline}
%	T \Delta U
%	+ U \dv[2]{T}{\zeta} + 2 \pdv{U}{\zeta} \dv{T}{\zeta}
%	+ k^2 TU
%	+ \varepsilon' \dv{\zeta} \qty(\frac{1}{\varepsilon'}) \pdv{UT}{\zeta} \\
%	= -p_{e \zeta} + \varepsilon' \pdv{\zeta} \qty(\frac{Q}{\varepsilon'})
%\end{multline}
%В итоге после преобразований имеем
\begin{equation}
	\Delta U 
        + \varepsilon' \dv{\zeta} \qty(\frac{1}{\varepsilon'})
	\frac{1}{T} \pdv{TU}{\zeta}
	+ k_{\text{эфф}}^2 U = 
	- \frac{1}{T} p^e_{e \zeta} 
	+ \frac{\varepsilon'}{T} \pdv{\zeta} \qty(\frac{Q}{\varepsilon'}),
\end{equation}
где
\begin{equation}
	k_{\text{эфф}}^2 = k^2 + \frac{1}{T} \dv[2]{T}{\zeta}.
\end{equation}

Аналогичным образом упрощается уравнение для магнитного вектора Герца
\begin{equation}
	\Pi_m = T(\zeta) V(\xi, \eta, \zeta),
\end{equation}
\begin{equation}
	\Delta V + k_{\text{эфф}}^2 V =
	- \frac{1}{T} p^m_{m \zeta} 
	+ \frac{1}{T} \pdv{M}{\zeta}.
\end{equation}

Как видно, в однородной среде уравнения для потенциалов Герца сводятся к уравнения Гельмгольца
\begin{equation}
\laplacian{u} + k^2 u = -f.
\end{equation}




\end{document}
